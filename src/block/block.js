/**
 * BLOCK: msender
 * Copyright (C) 2019 Raphaël Droz <raphaeld@animalequality.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY.
 * This is free software, and you are welcome to redistribute it
 * under certain conditions; type `show c' for details.
 *
 * A Gutenberg block to integrate with Msender.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

import { IntlProvider } from 'react-intl';
import Immutable, { Record } from 'immutable';
import { MsenderContainer, MSenderUI } from '@l214/msender/src/components/msender';
import { getMessagesLocale } from '@l214/msender/src/utils/i18n';
import Msender from '@l214/msender/src/models/msender';
import { getMessengers } from '@l214/msender/src/models/messenger';
import Recipient from '@l214/msender/src/models/recipient';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InspectorControls } = wp.editor;
const { TextControl, SelectControl, RadioControl, TextareaControl, FormTokenField, ToggleControl }  = wp.components;
const { Component } = wp.element;

class MSenderControl extends Component {
        constructor(props) {
                super(...arguments);
                this.props = props;
                this.state = { messengers: [], to: [] };
        }
        render() {
                const {
			attributes,
			setAttributes
                } = this.props;

                // lacks mailto && applemail
                const messengers = getMessengers().map(function(e) { return {value: e.get('identifier'), label: e.get('name')}; }).toJS();
                return (
                        <InspectorControls>
                          <label htmlFor="to">To</label>
                          <FormTokenField
                            value={ attributes.to }
                            suggestions={ [] }
                            onChange={ (to) => { this.setState( { to } ); this.props.setAttributes( { to: to.map(e => new Recipient({ email: e.trim() }))  } ); } }
                            placeholder="Email destination (To)"
                          />

                          <SelectControl
                            multiple
                            label={ __( 'Enabled mailers' ) }
                            value={ this.state.messengers }
                            onChange={ ( messengers ) => { this.setState( { messengers } ); } }
                            options={ messengers }
                          />

                          <TextareaControl
                            label="Message"
                            help="See the documentation for allowed placeholders."
                            value={ this.state.message }
                            onChange={ (value) => this.props.setAttributes( { message: value } ) }
                          />
                        </InspectorControls>
                );
        }
}

class WpMSender extends MsenderContainer {
        constructor(props) {
                super(...arguments);
                this.wp_props = props;
                this.props = props.attributes;
                this.state = {
                        msender: this.msenderFromProps(props)
                };
        }

        msenderFromProps(props) {
                return new Msender({
                        first_name: props.first_name,
                        last_name: props.last_name,
                        email: props.email,
                        message_to: props.attributes.to,
                        message_cc: props.attributes.cc,
                        message_bcc: props.attributes.bcc,
                        message_subject: props.subject,
                        message_text: props.message,
                        message_to_current: '',
                        filter_recipient: 'foo', // (props.filter_recipient ? props.filter_recipient : FILTER_RECIPIENT_ALL),
                        select_department: props.select_department,
                        step_two_title: props.step_two_title,
                        enable_mailchimp: (!!props.enable_mailchimp),
                        send_mailchimp: (!!props.send_mailchimp),
                        mailchimp_source: (props.mailchimp_source ? props.mailchimp_source : 'msender'),
                        locale: (props.locale ? props.locale : null),
                        translations: (props.translations ? Immutable.fromJS(props.translations) : null),
                        messengers: (props.messengers ? Immutable.Set(props.messengers) : null),
                });
        }

        render() {
                const { msender } = this.state;
                const { locale, messages } = getMessagesLocale(msender);
                return (
                        <div>
                          <MSenderControl {...this.wp_props}
                          />
                          <IntlProvider
                            locale={locale}
                            messages={messages}
                          >
                            <MSenderUI
                              msender={msender}
                              setIn={this.setInBound}
                              didGetPetitionData={this.didGetPetitionDataBound}
                            />
                          </IntlProvider>
                        </div>
                );
        }
}

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'animalequality/block-msender', {
	title: __( 'msender' ),
	icon: 'dashicons-email-alt',
	category: 'embed',
	keywords: [
		__( 'msender' ),
	],
        attributes: {
                "to": { type: 'array' },
                "cc": { type: 'array' },
                "bcc": { type: 'array' },
                "subject": { type: 'string' },
                "message": { type: 'string' },
                // "recipients": {}, // source? randomize?
                // "step_two_title": "todo",
                // "mailchimp": {}, // slug, default-on
                // "locale": "en-EN"
                "messengers": { type: 'array' },
        },

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: WpMSender,

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: WpMSender,
} );
